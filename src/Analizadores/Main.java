package Analizadores;
import java.io.*;
import java_cup.runtime.*;

import ast.*;

public class Main {

	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception
	{
		// TODO Auto-generated method stub
		
		SymbolFactory sf = new DefaultSymbolFactory();

		parser parser_obj;
		//Reader reader = new BufferedReader(new FileReader("fichero.txt"));
		//parser_obj=new parser(new Scanner(reader),sf);
		//parser_obj=new parser(new Scanner(System.in,sf),sf);
		parser_obj =new parser(new Scanner(new java.io.FileInputStream("src/Analizadores/fichero.h"),sf),sf);
		//System.out.println("token");
		parser_obj.parse();
		

		/*if (args.length==0) 
			parser_obj=new parser(new Scanner(System.in,sf),sf);
		else 
			parser_obj=new parser(new Scanner(new java.io.FileInputStream(args[0]),sf),sf);*/
		NodoBase root=parser_obj.action_obj.getASTroot();
		System.out.println();
		System.out.println("IMPRESION DEL AST GENERADO");
		System.out.println();
		ast.Util.imprimirAST(root);
		TablaSimbolos ts = new TablaSimbolos();
		ts.cargarTabla(root);
		ts.ImprimirClaves();




	}

}
