package Analizadores;


import java.util.*;


import ast.NodoAsignacion;
import ast.NodoBase;  //...
import ast.NodoDoWhile;
import ast.NodoEscribir;
import ast.NodoFor;
import ast.NodoFuncion;
import ast.NodoIdentificador;
import ast.NodoIf;
import ast.NodoLeer;
import ast.NodoOperacion;
import ast.NodoParametros; //falta
import ast.NodoProgram;
import ast.NodoValor;
import ast.NodoVariables;   
import ast.NodoWhile;




public class TablaSimbolos {
	/*
	 * tabla que guarda los identificadores*/
	private HashMap <String, RegistroSimbolo> tabla;	
	/*
	 * Contiene la linea inicial de la ejecucion del func*/
	private HashMap <String, Integer> tablaFunc;
	/*
	 * Tabla que me almacena el tipo de retorno de la funcion*/
	private HashMap <String, String> tablaTipo;
	private int direccion;  //Contador de las localidades de memoria asignadas a la tabla
	String tipoVar;
	public boolean error = false;
	
	/*
	 * Me va a almacenar los argumentos que recibe un proc o func
	 * 
	 * */
	//public ArrayList<String> listaArgumentos;
	//private HashMap <String, ArrayList<String>> tablaDeArgumentos;
	
	public TablaSimbolos() {
		super();
		tabla = new HashMap <String, RegistroSimbolo>();
		tablaFunc = new HashMap <String, Integer>();
		direccion=0;
	}

	public void cargarTabla(NodoBase raiz){
		while (raiz != null) {
			System.out.println("entroooooooooooooo");
	    if (raiz instanceof NodoIdentificador){
	    	InsertarSimbolo(((NodoIdentificador)raiz).getNombre(),-1,tipoVar, -1);
	    	//TODO: A�adir el numero de linea y localidad de memoria correcta
	    	
	    	System.out.println("NodoIdentificador");
	    }

	    /* Hago el recorrido recursivo */
	    if (raiz instanceof  NodoIf){
	    	cargarTabla(((NodoIf)raiz).getPrueba());
	    	cargarTabla(((NodoIf)raiz).getParteThen());
	    	if(((NodoIf)raiz).getParteElse()!=null){
	    		cargarTabla(((NodoIf)raiz).getParteElse());
	    	}
	    	System.out.println("Nodoif");
	    }
	    else if (raiz instanceof  NodoAsignacion){
	    	InsertarSimbolo(((NodoAsignacion)raiz).getIdentificador(),-1,tipoVar, -1);
	    	cargarTabla(((NodoAsignacion)raiz).getExpresion());
	    	System.out.println("NodoAsignacion");
	    }
	    else if (raiz instanceof  NodoEscribir){
	    	cargarTabla(((NodoEscribir)raiz).getExpresion());
	    	System.out.println("NodoEscribir");
	    }

	   /* else if (raiz instanceof  NodoLeer)
	    	InsertarSimbolo(((NodoLeer)raiz).getIdentificador(),-1);*/

	    else if (raiz instanceof NodoOperacion){
	    	cargarTabla(((NodoOperacion)raiz).getOpIzquierdo());
	    	cargarTabla(((NodoOperacion)raiz).getOpDerecho());
	    	System.out.println("NodoOperacion");
	    }
	    else if (raiz instanceof NodoParametros){
	    	cargarTabla(((NodoParametros)raiz).getTipo());
	    	cargarTabla(((NodoParametros)raiz).getListaParametros());
	    	System.out.println("NodoParametros");
	    }
	    else if (raiz instanceof  NodoWhile)
	    {
	    	cargarTabla(((NodoWhile)raiz).getPrueba());
	    	cargarTabla(((NodoWhile)raiz).getCuerpo());
	    	System.out.println("NodoWhile");
	    }
	    else if (raiz instanceof NodoVariables){
	    	InsertarSimbolo(((NodoVariables)raiz).getIdentificador(),-1,tipoVar, -1);
	    	if(((NodoVariables)raiz).getExpresion() != null){
	    			cargarTabla(((NodoVariables)raiz).getExpresion());
	    	}
	    	cargarTabla(((NodoVariables)raiz).getExpresion());
	    	System.out.println("NodoVariables");
	    }
	    else if (raiz instanceof NodoFor){
	    	cargarTabla(((NodoFor)raiz).getVariable1());
	    	cargarTabla(((NodoFor)raiz).getValorFinal());
	    	cargarTabla(((NodoFor)raiz).getVariable2());
	    	cargarTabla(((NodoFor)raiz).getCuerpo());
	    	System.out.println("NodoFor");
	    }
	    /*else if (raiz instanceof  NodoValor)
	    {
	    	cargarTabla(((NodoValor)raiz).getValor());
	    	cargarTabla(((NodoValor)raiz).getTipo());
	    }*/
	    else if (raiz instanceof  NodoDoWhile)
	    {
	    	cargarTabla(((NodoDoWhile)raiz).getCuerpo());
	    	cargarTabla(((NodoDoWhile)raiz).getPrueba());
	    	System.out.println("NodoDoWhile");
	    }

	    else if (raiz instanceof NodoProgram){
	    	if(((NodoProgram)raiz).getName_program()!=null){
	    		InsertarSimbolo(((NodoProgram)raiz).getName_program(),-1,null, -1);
	    		//cargarTabla(((NodoProgram)raiz).getName_program());
	    	}
	    	cargarTabla(((NodoProgram)raiz).getBody_program());
	    	System.out.println("NodoProgram");
	    }
	    else if (raiz instanceof NodoFuncion){
	    	String nombreFuncion = ((NodoFuncion)raiz).getName_function();
	    	InsertarSimbolo(nombreFuncion,-1,"hola", -1);
	    	if(((NodoFuncion)raiz).getArgs()!=null){
	    		cargarTabla(((NodoFuncion)raiz).getArgs());
	    	
	    	}
	    	cargarTabla(((NodoFuncion)raiz).getBody_function());
	    	System.out.println("NodoFuncion");
	    }
	    else if (raiz instanceof NodoVariables){
	    	if(((NodoFuncion)raiz).getArgs()!=null)
	    		cargarTabla(((NodoFuncion)raiz).getArgs());
	    	System.out.println("NodoVariables");
	    }

	    raiz = raiz.getHermanoDerecha();
	  }
	}
	
	//true es nuevo no existe se insertara, false ya existe NO se vuelve a insertar 
	public boolean InsertarSimbolo(String identificador, int numLinea, String tipo, int tamano){
		RegistroSimbolo simbolo;
		
		if(tabla.containsKey(identificador)){
			return false;
		}else{
			simbolo= new RegistroSimbolo(identificador,numLinea,direccion++);
			tabla.put(identificador,simbolo);					
			return true;	
		}
	}

	public RegistroSimbolo BuscarSimbolo(String identificador){
		RegistroSimbolo simbolo=(RegistroSimbolo)tabla.get(identificador);
		return simbolo;
	}
	
	public void ImprimirClaves(){
		System.out.println("*** Tabla de Simbolos ***");
		for( Iterator <String>it = tabla.keySet().iterator(); it.hasNext();) { 
            String s = (String)it.next();
	    System.out.println("Consegui Key: "+s+" con direccion: " + BuscarSimbolo(s).getDireccionMemoria());
		}
	}

	public boolean BuscarVariable(String identificador){
		if(tabla.containsKey(identificador)){
			return true;
		}else{
			return false;			
		}
	}
	
	public int getDireccion(String Clave){
		return BuscarSimbolo(Clave).getDireccionMemoria();
	}
	

	/*public String getTipo(String identificador) {
		RegistroSimbolo simbolo = this.tabla.get(identificador);
		return simbolo.getTipo();
	}

	
	public int getTamano(String Clave){
		return BuscarSimbolo(Clave).getTamano();
	}*/
	
	public boolean getError(){
		return error;
	}
	
	
	public String getTipoFuncion(String nomFunc) {
		return tablaTipo.get(nomFunc);
	}
	
	public void setTipoFuncion(String nomFunc, String tipo) {
		tablaTipo.put(nomFunc, tipo);
	}
	
	public void setLineaProcFunc(String nomFunc, Integer linea) {
		tablaFunc.put(nomFunc, linea);
	}
	
	public Integer getLineaProcFunc(String nomFunc) {
		return tablaFunc.get(nomFunc);
	}
	/*
	 * TODO:
	 * 1. Crear lista con las lineas de codigo donde la variable es usada.
	 * */
}
