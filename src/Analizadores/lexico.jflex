package Analizadores;
import java_cup.runtime.*;
%%
/* Habilitar la compatibilidad con el interfaz CUP para el generador sintactico*/
%cup
/* Llamar Scanner a la clase que contiene el analizador Lexico */
%class Scanner


/*-- DECLARACIONES --*/
%{
	public Scanner(java.io.InputStream r, SymbolFactory sf){
		this(r);
		this.sf=sf;
		lineanum=0;
		debug=true;
	}
	private SymbolFactory sf;
	private int lineanum;
	private boolean debug;
%}
%eofval{
    return sf.newSymbol("EOF",sym.EOF);
%eofval}


/* Acceso a la columna y fila actual de analisis CUP */
%line
%column

letra			= [a-zA-Z]
digito			= [0-9]
numero			= {digito}+
identificador	= {letra}({letra}|{numero}|"_")*
espacio			= [ \t]+
saltos			= \n | \n\r | \r\n
%%


//seccion de reglas lexicas
//aqui identificamos todas las expresiones regulares que deben ser aceptadas por el analizador 
//lexico


//Operadores condicionales


"if"            {	if(debug) System.out.println("token IF");
					return sf.newSymbol("IF",sym.IF);
				}

"else"          {	if(debug) System.out.println("token ELSE");
					return sf.newSymbol("ELSE",sym.ELSE);
				}				

//Operadores ciclicas


"for"			{	if(debug) System.out.println("token FOR");
					return sf.newSymbol("FOR",sym.FOR);
				}

"while"			{	if(debug) System.out.println("token WHILE");
					return sf.newSymbol("WHILE",sym.WHILE);
				}

"do"        	{	if(debug) System.out.println("token DO");
					return sf.newSymbol("DO",sym.DO);
				}
				

//Instruciones de lectura y escritura	

						
"gets"          {	if(debug) System.out.println("token GETS");
					return sf.newSymbol("GETS",sym.GETS);
				}
"puts"         {	if(debug) System.out.println("token PUTS");
					return sf.newSymbol("PUTS",sym.PUTS);
				}
				

//Instrucciones logicas


"&&"			{	if(debug) System.out.println("token AND");
					return sf.newSymbol("AND",sym.AND);
				}
"||"			{	if(debug) System.out.println("token OR");
					return sf.newSymbol("OR",sym.OR);
				}


//Operadores Aritmeticos


"="            {	if(debug) System.out.println("token ASSIGN");
					return sf.newSymbol("ASSIGN",sym.ASSIGN);
				}
"+"             {	if(debug) System.out.println("token PLUS");
					return sf.newSymbol("PLUS",sym.PLUS);
				}
"-"             {	if(debug) System.out.println("token MINUS");
					return sf.newSymbol("MINUS",sym.MINUS);
				}
"*"             {	if(debug) System.out.println("token TIMES");
					return sf.newSymbol("TIMES",sym.TIMES);
				}
"/"             {	if(debug) System.out.println("token OVER");
					return sf.newSymbol("OVER",sym.OVER);
				}
"/*"[^"*/"]*"*/"			{	if(debug) System.out.println("token COMENTARIO");
								return sf.newSymbol("COMENTARIO",sym.COMENTARIO);
							}


//Operadores Relacionales


"<"             {	if(debug) System.out.println("token LT");
					return sf.newSymbol("LT",sym.LT);
				}
"<="			{	if(debug) System.out.println("token LTQ");
					return sf.newSymbol("LTQ",sym.LTQ);
				}
">"				{	if(debug) System.out.println("token BT");
					return sf.newSymbol("BT",sym.BT);
				}
">="			{	if(debug) System.out.println("token BTQ");
					return sf.newSymbol("BTQ",sym.BTQ);
				}
"!="             {	if(debug) System.out.println("token EQ");
					return sf.newSymbol("EQ",sym.EQ);
				}
"<>"			{	if(debug) System.out.println("token DF");
					return sf.newSymbol("DF",sym.DF);
				}
				

//Demas Operadores o Palabras Reservadas


"void"			{	if(debug) System.out.println("token VOID");
					return sf.newSymbol("VOID",sym.VOID);
				}
"int"			{	if(debug) System.out.println("token INT");
					return sf.newSymbol("INT",sym.INT);
				}				
"bool"			{	if(debug) System.out.println("token BOOL");
					return sf.newSymbol("BOOL",sym.BOOL);
				}
"true"			{	if(debug) System.out.println("token TRUE");
					return sf.newSymbol("TRUE",sym.TRUE);
				}
"false"			{	if(debug) System.out.println("token FALSE");
					return sf.newSymbol("FALSE",sym.FALSE);
				}
"return"		{	if(debug) System.out.println("token RETURN");
					return sf.newSymbol("RETURN",sym.RETURN);
				}
"("             {	if(debug) System.out.println("token LPAREN");
					return sf.newSymbol("LPAREN",sym.LPAREN);
				}
")"             {	if(debug) System.out.println("token RPAREN");
					return sf.newSymbol("RPAREN",sym.RPAREN);
				}
";"             {	if(debug) System.out.println("token SEMI");
					return sf.newSymbol("SEMI",sym.SEMI);
				}
"{"				{	if(debug) System.out.println("token LLLAVE");
					return sf.newSymbol("LLLAVE",sym.LLLAVE);
				}
"}"				{	if(debug) System.out.println("token RLLAVE");
					return sf.newSymbol("RLLAVE",sym.RLLAVE);
				}
","				{	if(debug) System.out.println("token COLON");
					return sf.newSymbol("COLON",sym.COLON);
				}

//Palabras, comentarios, operaciones, espacio, saltos, retorno al carro, tabulacion


{numero}        {	if(debug) System.out.println("token NUM");
					return sf.newSymbol("NUM",sym.NUM,new String(yytext()));
				}
{identificador}	{	if(debug) System.out.println("token ID");
					return sf.newSymbol("ID",sym.ID,new String(yytext()));
				}

{saltos} 		{ lineanum++; }
{espacio}    	{ /* saltos espacios en blanco*/ }
.               { System.err.println("Caracter Ilegal encontrado en analisis lexico: " + yytext() + "\n"); }