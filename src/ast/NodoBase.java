package ast;

public class NodoBase {
	/*Esta clase fue creada para tener un origen comun de todas las clases...
	 * 
	 * ESTE MODELO DE AST PUEDE SER MEJORADO DE MUCHAS FORMAS, POR SIMPLIFICACION Y
	 * DIDACTICA EN CLASE, SE LLEVARA A CABO DE FORMA MUY SIMPLE Y POCO FLEXIBLE
	 * */
	
	private NodoBase HermanoDerecha;
	private String HermanoDerechaS;

	public NodoBase(NodoBase hermanoDerecha) {
		super();
		HermanoDerecha = hermanoDerecha;
	}

	public NodoBase(String hermanoDerechaS) {
		super();
		HermanoDerechaS = hermanoDerechaS;
	}

	public NodoBase() {
		super();
		HermanoDerecha=null;
	}

	public NodoBase getHermanoDerecha() {
		return HermanoDerecha;
	}

	public void setHermanoDerecha(NodoBase hermanoDerecha) {
		HermanoDerecha = hermanoDerecha;
	}
	
	public boolean TieneHermano() {
		return (HermanoDerecha!=null);
	}

	public String getHermanoDerechaS() {
		return HermanoDerechaS;
	}

	public void setHermanoDerechaS(String hermanoDerechaS) {
		HermanoDerechaS = hermanoDerechaS;
	}
	
	public boolean TieneHermanoS() {
		return (HermanoDerechaS!=null);
	}
	
}
