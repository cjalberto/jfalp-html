package ast;

public class NodoFor extends NodoBase{
	
	private NodoBase asignacion1;
	private NodoBase condicion;
	private NodoBase asignacion2;
	private NodoBase cuerpo;
	
	public NodoFor(){
		super();
		this.asignacion1 = null;
		this.condicion = null;
		this.asignacion2 = null;
		this.cuerpo = null;
	}
	
	public NodoFor(NodoBase asignacion1, NodoBase condicion, NodoBase asignacion2, NodoBase cuerpo){
		super();
		this.asignacion1 = asignacion1;
		this.condicion = condicion;
		this.asignacion2 = asignacion2;
		this.cuerpo = cuerpo;
	}

	public NodoBase getVariable1() {
		return asignacion1;
	}

	public void setVariable1(NodoBase asignacion1) {
		this.asignacion1 = asignacion1;
	}

	public NodoBase getValorFinal() {
		return condicion;
	}

	public void setValorFinal(NodoBase valorFinal) {
		this.condicion = valorFinal;
	}

	public NodoBase getVariable2() {
		return asignacion2;
	}

	public void setVariable2(NodoBase asignacion2) {
		this.asignacion2 = asignacion2;
	}

	public NodoBase getCuerpo() {
		return cuerpo;
	}

	public void setCuerpo(NodoBase cuerpo) {
		this.cuerpo = cuerpo;
	}
	
	

}
