package ast;

public class NodoFuncion extends NodoBase{
	
	private NodoBase type_function;
	private String name_function;
	private NodoBase args;
	private NodoBase body_function;
	
	public NodoFuncion()
	{
		super();
		this.type_function = null;
		this.name_function = "";
		this.args = null;
		this.body_function = null;
	}
	
	public NodoFuncion(NodoBase type_function, String name_function)
	{
		super();
		this.type_function = type_function;
		this.name_function = name_function;
		this.args = null;
		this.body_function = null;
	}
	
	public NodoFuncion(NodoBase type_function, String name_function, NodoBase body_function)
	{
		super();
		this.type_function = type_function;
		this.name_function = name_function;
		this.args = null;
		this.body_function = body_function;
	}

	public NodoFuncion(NodoBase type_function, String name_function, NodoBase args, NodoBase body_function)
	{
		super();
		this.type_function = type_function;
		this.name_function = name_function;
		this.args = args;
		this.body_function = body_function;
	}

	public NodoBase getType_function() {
		return type_function;
	}

	public void setType_function(NodoBase type_function) {
		this.type_function = type_function;
	}

	public String getName_function() {
		return name_function;
	}

	public void setName_function(String name_function) {
		this.name_function = name_function;
	}

	public NodoBase getArgs() {
		return args;
	}

	public void setArgs(NodoBase args) {
		this.args = args;
	}

	public NodoBase getBody_function() {
		return body_function;
	}

	public void setBody_function(NodoBase body_function) {
		this.body_function = body_function;
	}
		
}
