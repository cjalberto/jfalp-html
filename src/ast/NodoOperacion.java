package ast;

public class NodoOperacion extends NodoBase {
		
	private NodoBase opIzquierdo;
	private NodoBase opDerecho;
	private NodoBase operacion_a;
	private tipoOp operacion;
	
	public NodoOperacion(NodoBase opIzquierdo, tipoOp tipoOperacion, NodoBase opDerecho) {
		super();
		this.opIzquierdo = opIzquierdo;
		this.opDerecho = opDerecho;
		this.operacion = tipoOperacion;
		this.operacion_a = null;
	}

	public NodoOperacion(NodoBase opIzquierdo, NodoBase operacion_a, NodoBase opDerecho) {
		super();
		this.opIzquierdo = opIzquierdo;
		this.opDerecho = opDerecho;
		this.operacion = null;
		this.operacion_a = operacion_a;
	}

	public NodoOperacion(tipoOp tipoOperacion) {
		super();
		this.opIzquierdo = null;
		this.opDerecho = null;
		this.operacion = tipoOperacion;
		this.operacion_a = null;
	}

	public NodoBase getOpIzquierdo() {
		return opIzquierdo;
	}

	public void setOpIzquierdo(NodoBase opIzquierdo) {
		this.opIzquierdo = opIzquierdo;
	}

	public NodoBase getOpDerecho() {
		return opDerecho;
	}

	public void setOpDerecho(NodoBase opDerecho) {
		this.opDerecho = opDerecho;
	}

	public tipoOp getOperacion() {
		return operacion;
	}

	public void setOperacion(tipoOp operacion) {
		this.operacion = operacion;
	}

	public NodoBase getOperaciona() {
		return operacion_a;
	}

	public void setOperaciona(NodoBase operacion_a) {
		this.operacion_a = operacion_a;
	}
	
}
