package ast;

public class NodoParametros extends NodoBase{
	
	private NodoBase tipo;
	private String id;
	private NodoBase listaparametros;
	
	public NodoParametros(NodoBase tipo, String id, NodoBase listaparametros) {
		super();
		this.tipo = tipo;
		this.id = id;
		this.listaparametros = listaparametros;
	}

	public NodoParametros() {
		super();
		this.tipo = null;
		this.id = null;
		this.listaparametros = null;
	}

	public NodoBase getTipo() {
		return tipo;
	}

	public void setTipo(NodoBase tipo) {
		this.tipo = tipo;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public NodoBase getListaParametros() {
		return listaparametros;
	}

	public void setListaParametros(NodoBase listaparametros) {
		this.listaparametros = listaparametros;
	}
}
