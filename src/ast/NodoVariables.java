package ast;

public class NodoVariables extends NodoBase {
	private String identificador;
	private NodoBase expresion;
	
	public NodoVariables(String identificador) {
		super();
		this.identificador = identificador;
		this.expresion = null;
	}
	
	public NodoVariables(String identificador, NodoBase expresion) {
		super();
		this.identificador = identificador;
		this.expresion = expresion;
	}

	public NodoVariables() {
		super();
		this.identificador = null;
		this.expresion = null;
	}

	public String getIdentificador() {
		return identificador;
	}

	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}

	public NodoBase getExpresion() {
		return expresion;
	}

	public void setExpresion(NodoBase expresion) {
		this.expresion = expresion;
	}
	
}
