
//----------------------------------------------------
// The following code was generated by CUP v0.11a beta 20060608
// Fri Dec 09 14:58:23 VET 2016
//----------------------------------------------------

package Analizadores;

/** CUP generated class containing symbol constants. */
public class sym {
  /* terminals */
  public static final int DF = 22;
  public static final int BTQ = 20;
  public static final int OVER = 15;
  public static final int LPAREN = 29;
  public static final int SEMI = 31;
  public static final int INT = 24;
  public static final int FOR = 4;
  public static final int MINUS = 13;
  public static final int RPAREN = 30;
  public static final int AND = 9;
  public static final int LLLAVE = 32;
  public static final int LT = 17;
  public static final int OR = 10;
  public static final int BOOL = 25;
  public static final int RLLAVE = 33;
  public static final int NUM = 35;
  public static final int PLUS = 12;
  public static final int ASSIGN = 11;
  public static final int IF = 2;
  public static final int ID = 36;
  public static final int EOF = 0;
  public static final int RETURN = 28;
  public static final int TRUE = 26;
  public static final int error = 1;
  public static final int COMENTARIO = 16;
  public static final int BT = 19;
  public static final int VOID = 23;
  public static final int EQ = 21;
  public static final int TIMES = 14;
  public static final int COLON = 34;
  public static final int PUTS = 8;
  public static final int ELSE = 3;
  public static final int WHILE = 5;
  public static final int GETS = 7;
  public static final int FALSE = 27;
  public static final int LTQ = 18;
  public static final int DO = 6;
}

