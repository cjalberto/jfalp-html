package ast;

public class NodoAsignacion extends NodoBase {
	private String identificador;
	private String tipo2;
	private NodoBase expresion;
	private tipoOp tipo;
	
	public NodoAsignacion(String identificador) {
		super();
		this.identificador = identificador;
		this.expresion = null;
		this.tipo = null;
	}
	
	public NodoAsignacion(String identificador, NodoBase expresion) {
		super();
		this.identificador = identificador;
		this.expresion = expresion;
		this.tipo = null;
	}

	public NodoAsignacion(tipoOp tipo, NodoBase expresion) {
		super();
		this.identificador = null;
		this.expresion = expresion;
		this.tipo = tipo;
	}

	public NodoAsignacion(String tipo, String identificador, NodoBase expresion) {
		super();
		this.identificador = identificador;
		this.expresion = expresion;
		this.tipo2 = tipo;
	}

	public String getIdentificador() {
		return identificador;
	}

	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}

	public NodoBase getExpresion() {
		return expresion;
	}

	public void setExpresion(NodoBase expresion) {
		this.expresion = expresion;
	}
	
	public NodoBase getTipo() {
		return expresion;
	}

	public void setTipo(tipoOp tipo) {
		this.tipo = tipo;
	}
	
}
